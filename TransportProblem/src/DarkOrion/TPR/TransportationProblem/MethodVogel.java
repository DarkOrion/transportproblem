package DarkOrion.TPR.TransportationProblem;

import java.util.ArrayList;
import java.util.Collections;

class MethodVogel extends InitialSolution {
	
	ArrayList<Integer> tmpCell = new ArrayList<Integer>();
	ArrayList<Integer> deltaLine = new ArrayList<Integer>();
	ArrayList<Integer> deltaColumn = new ArrayList<Integer>();
	int iL, jC, min; //������� ������ ��������� �� ������ ������
	
	MethodVogel(TableTP tableTP) {
		super(tableTP);
	}
	
	public boolean isOptimalSolution() {
		return false;
	}

	//������� ������� ������� ������
	public TableTP findSolution() {
		super.tableTP = tableTP;
		super.amountP = tableTP.getAmountProv();
		super.amountC = tableTP.getAmountCons();
		int maxDeltaL, maxDeltaC;
		
		do {	
			deltaLine.clear();
			deltaColumn.clear();
			
			findDeltaLine();  //������� ������� ����������� ��������� �� �������			
			findDeltaColumn();	//������� ������� ����������� ��������� �� ��������
			
			maxDeltaL = Collections.max(deltaLine);  //����� ������������ ������� ����������� ���������
			iL = deltaLine.indexOf(maxDeltaL);		//�� �������

			maxDeltaC = Collections.max(deltaColumn);  //����� ������������ ������� ����������� ���������
			jC = deltaColumn.indexOf(maxDeltaC);	   //�� ��������
			
			if (maxDeltaL > maxDeltaC)
				findMinLine();   	  //� ������ ��� ������� � ������������ �������� ���� ���������� �����
			else					  //������� ����������� ����� 
				findMinColumn();

			distributeProducts(iL,jC);  //������������� ����� ����� ����������� � ������������
			
		} while (isThereStocksOrNeeds());
		
		fictitiousProducts(); //���� ������� ����� ������������ �� ���������� ����������(�����������)
		
		min=0; iL=0; jC=0; deltaLine.clear(); deltaColumn.clear(); tmpCell.clear();
		
		return tableTP;
	}
	
	//������� ������� ����������� ��������� �� �������
	private void findDeltaLine() {
		for (int i=0; i<amountP; i++) {
			for (int j=0;j<amountC; j++)
				//���� ������� ����� � �����������
				if (tableTP.getBalanceOfProvider(i) > 0 && tableTP.getBalanceOfConsumer(j) > 0) 
					tmpCell.add(tableTP.getCostOfTransportation(i, j));
			switch (tmpCell.size()) {
			case 0:
				deltaLine.add(-1);
				break;
			case 1:
				deltaLine.add(0);
				break;
			default: //���� ������ ���� �� ������� ������� ����� ������������ ��������
				Collections.sort(tmpCell);
				deltaLine.add(tmpCell.get(1) - tmpCell.get(0));
				break;
			}
			tmpCell.clear();		
		}
	}
	
	//������� ������� ����������� ��������� �� ��������
	private void findDeltaColumn() {
		for (int j=0; j<amountC; j++) {
			for (int i=0; i<amountP; i++)
				//���� ������� ����� � �����������
				if (tableTP.getBalanceOfProvider(i) > 0 && tableTP.getBalanceOfConsumer(j) > 0) 
					tmpCell.add(tableTP.getCostOfTransportation(i, j));
			switch (tmpCell.size()) {
			case 0:
				deltaColumn.add(-1);
				break;
			case 1:
				deltaColumn.add(0);
				break;
			default: //���� ������ ���� �� ������� ������� ����� ������������ ��������
				Collections.sort(tmpCell);
				deltaColumn.add(tmpCell.get(1) - tmpCell.get(0));
				break;
			}
			tmpCell.clear();
		}
	}
	
	//������� ����������� ����� � ������
	private void findMinLine() {
		min=Integer.MAX_VALUE;
		for (int j=0; j<amountC; j++) 
			if (deltaColumn.get(j) >= 0) 
				if (min > tableTP.getCostOfTransportation(iL, j)) {
					min = tableTP.getCostOfTransportation(iL, j);
					jC = j;
				}		
	}
	
	//������� ����������� ����� � �������
	private void findMinColumn() {
		min=Integer.MAX_VALUE;
		for (int i=0; i<amountP; i++) 
			if (deltaLine.get(i) >= 0) 
				if (min > tableTP.getCostOfTransportation(i, jC)) {
					min = tableTP.getCostOfTransportation(i, jC);
					iL = i;
				}
	}
	
	//��������� �������� �� ���������������� ������ ��� �����������
	private boolean isThereStocksOrNeeds() {
		int ni=0, nj=0;
		for (int i=0; i<amountP; i++)
			if (tableTP.getBalanceOfProvider(i) > 0)
				ni++;
		for (int j=0; j<amountC; j++) 
			if (tableTP.getBalanceOfConsumer(j) > 0)
				nj++;
		return !(ni==0 || nj==0);
	}

}
