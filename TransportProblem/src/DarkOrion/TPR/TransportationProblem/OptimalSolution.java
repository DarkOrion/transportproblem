package DarkOrion.TPR.TransportationProblem;

public abstract class OptimalSolution implements MethodOfSolution{
	
	TableTP tableTP; //�������
	int amountP;
	int amountC;
	
	OptimalSolution (TableTP tableTP) {
		this.tableTP = tableTP;
		amountP = tableTP.getAmountProv();
		amountC = tableTP.getAmountCons();
	}

	public static OptimalSolution PotentialMethod(TableTP tableTP) {

		return new PotentialMethod(tableTP);
	}
	
	public abstract boolean isOptimalSolution();

	public abstract TableTP findSolution();
}
