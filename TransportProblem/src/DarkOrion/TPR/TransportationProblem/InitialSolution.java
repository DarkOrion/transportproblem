package DarkOrion.TPR.TransportationProblem;

public abstract class InitialSolution implements MethodOfSolution {
	
	TableTP tableTP; //�������
	int amountP;
	int amountC;
	
	private int provProd = 0; //����� ������� ���������
	private int consProd = 0; //����� ����������� � ���������
	
	InitialSolution(TableTP tableTP) {
		
		int supplies[], needs[], cost[][];
		int amountPOld = tableTP.getAmountProv();
		int amountCOld = tableTP.getAmountCons();
		for (int i=0; i<amountPOld; i++)
			provProd += tableTP.getProductsProvider(i);
		for (int j=0; j<amountCOld; j++)
			consProd += tableTP.getProductsConsumer(j);
		//���� ������ ������ ��� �����������
		if (provProd > consProd) {
			//��������� ���������� �����������
			supplies = new int [amountPOld];
			needs = new int [amountCOld + 1];
			cost = new int [amountPOld][amountCOld + 1];
			needs[amountCOld] = 0;
			for (int i=0; i<amountPOld; i++) 
				cost[i][amountCOld] = 0;
		}
		//���� ����������� ������ ��� �����
		else if (provProd < consProd) {
			//��������� ���������� ����������
			supplies = new int [amountPOld + 1];
			needs = new int [amountCOld];
			cost = new int [amountPOld + 1][amountCOld];
			supplies[amountPOld] = 0;
			for (int j=0; j<amountCOld; j++) 
				cost[amountPOld][j] = 0;		
		}
		
		else {
			supplies = new int [amountPOld];
			needs = new int [amountCOld];
			cost = new int [amountPOld][amountCOld];
		}
		
		for (int i=0; i<amountPOld ; i++)
			for (int j=0; j<amountCOld; j++)
				cost[i][j] = tableTP.getCostOfTransportation(i, j);
		//� ������ ���������� ������� �����
		for (int i=0; i<amountPOld; i++)
			supplies[i] = tableTP.getProductsProvider(i);
		//� ������ ����������� ������ ����������� 
		for (int j=0; j<amountCOld; j++)
			needs[j] = tableTP.getProductsConsumer(j);
		
		
		this.tableTP = new TableTP(supplies, needs, cost);
		this.amountP = this.tableTP.getAmountProv();
		this.amountC = this.tableTP.getAmountCons();
	}
	
	
	
	public static InitialSolution MethodVogel(TableTP tableTP) {
		
		return new MethodVogel(tableTP);
	}
	
	public static InitialSolution NorthwestCorner(TableTP tableTP) {

		return new NorthwestCorner(tableTP);
	}
	
	public abstract boolean isOptimalSolution();
	
	public abstract TableTP findSolution();
	
	
	//������������� ����� ����� ����������� � ������������
	void distributeProducts(int i, int j) {
		
		int resSup=0, resNeed=0, minRes=0;
		resSup = tableTP.getBalanceOfProvider(i);
		resNeed = tableTP.getBalanceOfConsumer(j);
		//������ ���� ������ - ������ ��� �����������
		minRes = Math.min(resNeed, resSup);
		//������ ����� �������
		tableTP.setBalanceOfProviders(i, resSup - minRes); 
		tableTP.setBalanceOfConsumer(j, resNeed - minRes);
		//����������� ���������� ������������ ������ �� ������� ����
		tableTP.addProductsTransported(minRes, i, j);
		//cellTable[i][j].setCellReference(true);
	}
	
	void fictitiousProducts() { //���� ������� ����� ������������ �� ���������� ����������(�����������)
		
		//���� ������ ������ ��� �����������
		if (provProd > consProd) {
			//� ������ ���������� ����������� ������� ������������������ ������
			tableTP.setBalanceOfConsumer(amountC-1, provProd - consProd);
			for (int i=0; i<amountP; i++) 
				distributeProducts(i, amountC-1);						
		}	
		//���� ����������� ������ ��� �����
		if (provProd < consProd) {
			tableTP.setBalanceOfProviders(amountP-1, consProd - provProd);
			for (int j=0; j<amountC; j++) 
				distributeProducts(amountP-1, j);
		}
	}
}
