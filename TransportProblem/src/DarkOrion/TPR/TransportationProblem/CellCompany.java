package DarkOrion.TPR.TransportationProblem;

class CellCompany {
	
	final private int amountProd; //������ ��� �����������
	private int resProd;    //������� ������ ��� �����������
	
	CellCompany (int amountProd) {
		this.amountProd = amountProd;
		resProd = amountProd;
	}
	
	int getAmountProducts() {
		return amountProd;
	}
	
	int getResProducts() {
		return resProd;
	}

	void setResProducts(int resProd) {
		this.resProd = resProd;
	}
}
