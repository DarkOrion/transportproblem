package DarkOrion.TPR.TransportationProblem;

public interface MethodOfSolution {
	
	public boolean isOptimalSolution();
	public TableTP findSolution();
}
