package DarkOrion.TPR.TransportationProblem;

class CellTable {
	
	final private int costTransp; //��������� ���������
	private int prodTransp;       //����������� ������
	
	CellTable (int costTransp) {
		this.costTransp = costTransp;
		prodTransp = 0;
	}
	
	//��������� ���������� ������������ ������ �� ������� ����
	void addProdTransp (int prod) {
		prodTransp = prodTransp + prod;
	}
	
	//���������� ���������� ������, ������� ����������� �� ����� ����
	int getProdTransp() {
		return prodTransp;
	}
	//���������� ��������� ���������
	int getCostTransp() {
		return costTransp;
	}
}