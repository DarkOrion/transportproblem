package DarkOrion.TPR.TransportationProblem;


import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;


class PotentialMethod extends OptimalSolution {
	
	private int U[];
	private int V[];
	private int delta[][];
	private Deque<PositionElement> cycle;
	private PositionElement pE;
	private boolean cellInCycle[][];
	
	private final int NOT_CAlCULATED = Integer.MAX_VALUE;
	
	PotentialMethod(TableTP tableTP) {
		super(tableTP);
		U = new int[amountP];
		V = new int[amountC];
		delta = new int[amountP][amountC];
		cycle = new ArrayDeque<PositionElement>();
		cellInCycle = new boolean[amountP][amountC];
		for (int i=0; i<amountP; i++) 
			for (int j=0; j<amountC; j++) 
				cellInCycle[i][j] = false;
	}

	
	public boolean isOptimalSolution() {	
		//������ �������� �� ������� �����������
		calcPotentials(); //����� ����������
		assessmentRoute(); 	//����� ������ ���������������� ��������
		negativeAssessment(); //����� ������ � ���������� ������������� ������� ��������
		pE = cycle.getFirst();
		return delta[pE.i][pE.j] >= 0;
	}

	public TableTP findSolution() {
		redistributeGoods();
		return tableTP;
	}
	
	
	
	//������� ����������
	private void calcPotentials () {

		//����������� ��������� 0
		Arrays.fill(U, NOT_CAlCULATED);
		Arrays.fill(V, NOT_CAlCULATED);
		U[0] = 0;
		//��������� ���� �� ����� ���������� ��� ����������
		while (isNotCalculated()) {
			for (int i=0; i<amountP; i++)
				if (U[i] != NOT_CAlCULATED)
					goColumn(i);
		}
	}
	
	//������ ��� �� ���������� ����������
	private boolean isNotCalculated() {
		for (int i=0; i<amountP; i++)
			if (U[i] == NOT_CAlCULATED)
				return true;
		for (int j=0; j<amountC; j++)
			if (V[j] == NOT_CAlCULATED)
				return true;
		return false;			
	}

	
	//��������� ��������� ����������� ���� ����� ��������� ����������
	private void goColumn (int i) {
		for (int j=0; j<amountC; j++)
			//�������� �� ���� ������������. ���� ������ ������� ������� � ��������� 
			//����������� ��� �� ��������, �� ��������� ��������� �����������
			if (tableTP.getProductsTransported(i, j) > 0  && V[j] == NOT_CAlCULATED) {
				V[j] = tableTP.getCostOfTransportation(i, j) - U[i];
				goLine(j); //��������� �� ������
			}
	}
	
	//��������� ��������� ���������� ���� ����� ��������� �����������
	private void goLine (int j) {
		for (int i=0; i<amountP; i++)
			//�������� �� ���� �����������. ���� ������ ������� ������� � ���������
			//���������� �� ��������, �� ��������� ��������� ����������
			if (tableTP.getProductsTransported(i, j) > 0 && U[i] == NOT_CAlCULATED) {
				U[i] = tableTP.getCostOfTransportation(i, j) - V[j];
				goColumn(i); //��������� �� �������
			}
	}
	
	//����� ������ ���������������� ��������
	public void assessmentRoute() {
		for (int i=0; i<amountP; i++)
			for (int j=0; j<amountC; j++)
				if (tableTP.getProductsTransported(i, j) == 0) //���� ������ �� �������
					//������ ������������������ �������� = ����� �������� - ( ��������� ���������� + ��������� ����������� )
					delta[i][j] = tableTP.getCostOfTransportation(i, j) - U[i] - V[j];
	}
	
	
	//������� ������ � ���������� ������������� ������� ��������
	public void negativeAssessment() {
		int iL=0, jC=0;
		for (int i=0, min=Integer.MAX_VALUE; i<amountP; i++)
			for (int j=0; j<amountC; j++)
				if (tableTP.getProductsTransported(i, j) == 0) //���� ������ �� �������
					if (min > delta[i][j]) {
						min = delta[i][j];
						iL = i; jC = j;
					}
		//��� ������ ��������� ��� �����
		cycle.push(new PositionElement(iL,jC));
		cellInCycle[iL][jC] = true;
	}
	
//************************������ ���� � ���������������� ������*********************************
	public void redistributeGoods() {
		int minCycle = 0;

		cycleGoLine(pE.j,0);
		
		int n=1;
		for (PositionElement pECycle : cycle) {
			if (n % 2 != 0) {
				if (n == 1)
					minCycle = tableTP.getProductsTransported(pECycle.i, pECycle.j);
				else if (minCycle > tableTP.getProductsTransported(pECycle.i, pECycle.j))
					minCycle = tableTP.getProductsTransported(pECycle.i, pECycle.j);
			}
			n++;
		}
		n=1;
		//���������������� ������
		for (PositionElement pECycle : cycle) {
			if (n % 2 == 0)
				tableTP.addProductsTransported(minCycle, pECycle.i, pECycle.j);
			else
				tableTP.addProductsTransported(-minCycle, pECycle.i, pECycle.j);
			n++;
		}
			
	}
	
	private void cycleGoLine (int jC, int start) {
		int iL = -1;
		//������� ������� ������, ������� ��� �� �������� ������ �����
		for (int i=start; i<amountP; i++)
			if (!cellInCycle[i][jC] && tableTP.getProductsTransported(i, jC) > 0) {
				cellInCycle[i][jC] = true;
				iL = i;
				break;
			}
		//���� ������ �����, �� ��������� �� � ����, � ��������� �� ������
		if (iL != -1) {
			cycle.push(new PositionElement(iL,jC));
			cycleGoColumn(iL,0);
		}
		//���� �� �����, ������� ���������� ������ �� ����� � ���� ������
		//��� ���� ���� ���������� � ������, ������� ���� ����� ���
		else {
			pE = cycle.pop();
			cellInCycle[pE.i][pE.j] = false;
			cycleGoColumn(pE.i, pE.j+1);
		}
	}
	
	private void cycleGoColumn (int iL, int start) {
		int jC = -1;
		//������� ������� ������, ������� ��� �� �������� ������ �����
		for (int j=start; j<amountC; j++) {
			if (!cellInCycle[iL][j] && tableTP.getProductsTransported(iL, j) > 0) {
				cellInCycle[iL][j] = true;
				jC = j;
				break;
			}
			else if (cycle.getLast().i == iL && cycle.getLast().j == j)
				return;
		}
		//���� ������ �����, �� ��������� �� � ����, � ��������� �� ������
		if (jC != -1) {
			cycle.push(new PositionElement(iL,jC));
			cycleGoLine(jC,0);
		}
		//���� �� �����, ������� ���������� ������ �� ����� � ���� ������
		//��� ���� ���� ���������� � ������, ������� ���� ����� ���
		else {
			pE = cycle.pop();
			cellInCycle[pE.i][pE.j] = false; 
			cycleGoLine(pE.j, pE.i+1);
		}
	}

}
