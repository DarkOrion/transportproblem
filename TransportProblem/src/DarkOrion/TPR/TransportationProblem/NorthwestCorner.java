package DarkOrion.TPR.TransportationProblem;

public class NorthwestCorner extends InitialSolution {
	
	
	NorthwestCorner(TableTP tableTP) {
		super(tableTP);
	}
	
	public boolean isOptimalSolution() {
		return false;
	}

	//������� ������� � ������� ������ ������-��������� ����
	public TableTP findSolution() {
		super.tableTP = tableTP;
		super.amountP = tableTP.getAmountProv();
		super.amountC = tableTP.getAmountCons();
		int i=0,j=0;
		do {
			
			distributeProducts(i, j); //������������� ����� �� ������ ����
			
			if (tableTP.getBalanceOfProvider(i) == 0)
				i++; //���� �� �������� ������ ������
				//��������� �� ��������� ������
			if (tableTP.getBalanceOfConsumer(j) == 0)
				j++; //���� �� �������� ����������� ������
				//��������� � ���������� �������
			} while (i < amountP && j < amountC);
		
		fictitiousProducts(); //���� ������� ����� ������������ �� ���������� ����������(�����������)
		return tableTP;
	}
}
