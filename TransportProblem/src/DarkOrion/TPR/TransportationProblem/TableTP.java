package DarkOrion.TPR.TransportationProblem;

public class TableTP {
	private int amountP; //���������� �����������
	private int amountC; //���������� ������������
	
	private CellTable [][] cellTable; //������ �������
	private CellCompany [] cellProv; //������ ����������
	private CellCompany [] cellCons; //������ �����������
	
	
	public TableTP(int[] supplies, int[] needs, int[][] cost) {
		
		amountP = supplies.length;
		amountC = needs.length;
		//� ������ ������� ������� ��������� ���������
		cellTable = new CellTable[amountP][amountC];
		cellProv = new CellCompany[amountP];
		cellCons = new CellCompany[amountC];
		for (int i=0; i<amountP ; i++)
			for (int j=0; j<amountC; j++)
				cellTable[i][j] = new CellTable(cost[i][j]);
		//� ������ ���������� ������� �����
		for (int i=0; i<amountP; i++)
			cellProv[i] = new CellCompany(supplies[i]);
		//� ������ ����������� ������ ����������� 
		for (int j=0; j<amountC; j++)
			cellCons[j] = new CellCompany(needs[j]);
	}
	
	public int getAmountProv() {
		return amountP;
	}
	
	public int getAmountCons() {
		return amountC;
	}
	
	//��������� ���������
	int getCostOfTransportation(int i, int j) {
		return cellTable[i][j].getCostTransp();
	}
	
	//����������� ���������
	public int getProductsTransported(int i, int j) {
		if (i<0 || j<0)
			throw new java.lang.IllegalArgumentException("i<0 or j<0 � ������ productsTransported");
		return cellTable[i][j].getProdTransp();
	}
	
	//��������� ���-�� ������ �� ������ ����
	void addProductsTransported(int prod, int i, int j) {
		cellTable[i][j].addProdTransp(prod);
	}
	
	int getProductsProvider(int i) {
		return cellProv[i].getAmountProducts();
	}
	
	int getProductsConsumer(int j) {
		return cellCons[j].getAmountProducts();
	}
	
	//������� �������� ����������
	public int getBalanceOfProvider(int i) {
		if (i<0)
			throw new java.lang.IllegalArgumentException("i<0 � ������ residueProductsProvider");
		return cellProv[i].getResProducts();
	}
	
	//��������� ���������� ������ � ����������
	void setBalanceOfProviders(int i, int resProd) {
		cellProv[i].setResProducts(resProd);
	}
	
	//������� �������� �����������
	public int getBalanceOfConsumer(int j) {
		if (j<0)
			throw new java.lang.IllegalArgumentException("j<0 � ������ residueProductsConsumer");
		return cellCons[j].getResProducts();
	}
	
	//��������� ���������� ����������� � ����������
	void setBalanceOfConsumer(int j, int resProd) {
		cellCons[j].setResProducts(resProd);
	}
	
}

