package DarkOrion.TPR.TransportationProblem;

public class MethodsForTable {
	
	private MethodsForTable(){}
	
	public static int costDelivery(TableTP tableTP) {	//������� ����� ��������� ��������� 
		if (tableTP == null)
			throw new NullPointerException("TableTP is null at method costDelivery");
		int S = 0;
		for (int i=0; i < tableTP.getAmountProv(); i++)
			for (int j=0; j < tableTP.getAmountCons(); j++)
				S += tableTP.getProductsTransported(i, j) * tableTP.getCostOfTransportation(i, j);
		return S;
	}
	
	//�� ����������� �� ����
	public static boolean isNotDegenerate(TableTP tableTP) {
		if (tableTP == null)
			throw new NullPointerException("TableTP is null at method isNotDegenerate");
		int n = 0;
		int amountP = tableTP.getAmountProv();
		int amountC = tableTP.getAmountCons();
		//������� ���������� �������� �����
		for (int i=0; i < amountP; i++)
			for (int j=0; j < amountC; j++)
				if (tableTP.getProductsTransported(i, j) > 0)
					n++;
		return (n >= amountP + amountC - 1 ); //���� �������� ����� �� ������ ��� m+n-1
	}
}
