package DarkOrion.TPR.TransportationProblem;

class PositionElement {
	final public int i;
	final public int j;
	
	PositionElement (int i, int j) {
		this.i = i;
		this.j = j;
	}
}
