package DarkOrion.TPR.TransportationProblemGUI;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import DarkOrion.TPR.TransportationProblem.InitialSolution;
import DarkOrion.TPR.TransportationProblem.MethodOfSolution;
import DarkOrion.TPR.TransportationProblem.TableTP;

class TablePrimPanel extends JPanel {
	
	private int amountP; //���������� �����������
	private int amountC; //� ������������
	private TableTP tableTP;
	private MethodOfSolution methodOfSolution; //��������� ������� �������
	
	private TableResPanel resultPanel;
	
	final private JLabel labelProv;
	final private JLabel labelCons;
	final private JLabel labelSupplies;
	final private JLabel labelNeed;
	private JLabel [] labelEachProv; //�������� �����������
	private JLabel [] labelEachCons; //�������� ������������
	
	private JTextField [] textSupplies; //����� �����������
	private JTextField [] textNeeds; //����������� ������������
	private JTextField [][] textCostTransporting; //��������� ��������
	
	private boolean decideVogel;
	private boolean decideNorthWest;
	
	private JButton buttonOk;
	
	TablePrimPanel() {
		labelProv = new JLabel("������.");
		labelCons = new JLabel("������.");
		labelSupplies = new JLabel("�����");
		labelNeed = new JLabel("�����������");

		buttonOk = new JButton("Ok");
		
		//********************������� ���� ��� ������� ������ ��***************************
		
		buttonOk.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				enterData();	//������ � ������� ������ ��������� �������������
				
				if (decideVogel)
					methodOfSolution = InitialSolution.MethodVogel(tableTP);// ������� ������� ������� ������
					
				if (decideNorthWest)
					methodOfSolution = InitialSolution.NorthwestCorner(tableTP); //������� ������� ������� ������-��������� ����
				
				tableTP = methodOfSolution.findSolution(); //������ ��������� ������� �������
				
				resultPanel.createTableResult(tableTP); //������ ������� � ������� ������
			}

		});
		//************************************************************************************
	}
	
	void createTable(final int amountP,final int amountC, final TableResPanel resultPanel, boolean switchStatusVogel, boolean switchStatusNorth) {
		
		removeAll();
		this.amountP = amountP;
		this.amountC = amountC;
		this.resultPanel = resultPanel;
		decideVogel = switchStatusVogel;
		decideNorthWest = switchStatusNorth;
		
		addItems(); //��������� �������� �� panelTable
		
	}
	
	
	//������ � ������� ������ �������� �������������
	private void enterData () {

		try {
			int supplies[] = new int[amountP];
			int needs[] = new int[amountC];
			int costTransporting[][] = new int [amountP][amountC];
			for (int i=0; i<amountP; i++) {
				if (Integer.valueOf(textSupplies[i].getText()) <= 0) //���� ����� ������ 0
					throw new IllegalArgumentException();
				supplies[i] = Integer.valueOf(textSupplies[i].getText());
			}
			for (int j=0; j<amountC; j++) {
				if (Integer.valueOf(textNeeds[j].getText()) <= 0) //���� ����� ������ 0
					throw new IllegalArgumentException();
				needs[j] = Integer.valueOf(textNeeds[j].getText());
			}
			for (int i=0; i<amountP; i++)
				for (int j=0; j<amountC; j++) {
					if (Integer.valueOf(textCostTransporting[i][j].getText()) <= 0) //���� ����� ������ 0
						throw new IllegalArgumentException();
					costTransporting[i][j] = Integer.valueOf(textCostTransporting[i][j].getText());
				}
			//��������� ���������, ����� ���������� � ����������� �����������
			tableTP = new TableTP(supplies, needs, costTransporting);
		}
		catch(IllegalArgumentException err){
			JOptionPane.showMessageDialog(TablePrimPanel.this,
					"������� ����� ������ �����, ��������������� �����.", "�������� ���� ������", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
	
	//���������� �������� �� panelTable
	private void addItems() {
		//��������� ��������� ���� ��� ����� ��������� ���������
		textCostTransporting = new JTextField[amountP][amountC];
		//��������� ���� ��� ����� ������ � �����������
		textSupplies = new JTextField[amountP];
		textNeeds = new JTextField[amountC];
		//label � ��������� ����������� � ������������
		labelEachProv = new JLabel[amountP];
		labelEachCons = new JLabel[amountC];
		
		GridBagLayout gblPS = new GridBagLayout();
		setLayout(gblPS);
		GridBagConstraints GBC = new GridBagConstraints();
		//label ���������
		GBC.gridx = 0;
		GBC.gridy = 1;
		gblPS.setConstraints(labelProv, GBC);
		add(labelProv);
		//label �����������
		GBC.gridx = 1;
		GBC.gridy = 0;
		gblPS.setConstraints(labelCons, GBC);
		add(labelCons); 
		//label �����
		GBC.gridx = amountC+2;
		GBC.gridy = 0;
		GBC.insets = new Insets(20,20,0,0);
		gblPS.setConstraints(labelSupplies, GBC);
		add(labelSupplies);
		//label �����������
		GBC.gridx = 0;
		GBC.gridy = amountP+2;
		GBC.insets = new Insets(20,20,0,0);
		gblPS.setConstraints(labelNeed, GBC);
		add(labelNeed);
		
		//���������� �� ������ ���� ����������� � ������������
		for (int i=0; i<amountP; i++) {
			labelEachProv[i] = new JLabel("A"+(i+1));
			GBC.gridx = 0;
			GBC.gridy = i+2;
			GBC.insets = new Insets(10,10,0,0);
			gblPS.setConstraints(labelEachProv[i], GBC);					
			add(labelEachProv[i]);
			
			//��������� ���� ��� ����� ������ ����������
			textSupplies[i] = new JTextField(3);
			GBC.gridx = amountC+2;
			GBC.gridy = i+2;
			GBC.insets = new Insets(20,20,0,0);
			gblPS.setConstraints(textSupplies[i], GBC);					
			add(textSupplies[i]);
			
		}
		for (int j=0; j<amountC; j++) {
			labelEachCons[j] = new JLabel("B"+(j+1));
			GBC.gridx = j+2;
			GBC.gridy = 0;
			GBC.insets = new Insets(10,10,0,0);
			gblPS.setConstraints(labelEachCons[j], GBC);					
			add(labelEachCons[j]);
			
			//��������� ���� ��� ����� ����������� �����������
			textNeeds[j] = new JTextField(3);
			GBC.gridx = j+2;
			GBC.gridy = amountP+2;
			GBC.insets = new Insets(20,20,0,0);
			gblPS.setConstraints(textNeeds[j], GBC);					
			add(textNeeds[j]);
		}
		
		
		//��������� ��������� ���� ��� ����� ��������� ���������
		for (int i=0; i<amountP; i++)
			for (int j=0; j<amountC; j++) {
				textCostTransporting[i][j] = new JTextField(3);
				GBC.gridx = j+2;
				GBC.gridy = i+2;
				GBC.weightx = 0.1;
				gblPS.setConstraints(textCostTransporting[i][j], GBC);					
				add(textCostTransporting[i][j]);
			}	
		//��������� ������ ok
		GBC.gridx = amountC+2;
		GBC.gridy = amountP+2;
		gblPS.setConstraints(buttonOk, GBC);
		add(buttonOk);
		
		revalidate();
		repaint();
	}
	
}

