package DarkOrion.TPR.TransportationProblemGUI;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import DarkOrion.TPR.TransportationProblem.MethodOfSolution;
import DarkOrion.TPR.TransportationProblem.MethodsForTable;
import DarkOrion.TPR.TransportationProblem.OptimalSolution;
import DarkOrion.TPR.TransportationProblem.TableTP;

class TableResPanel extends JPanel {
	
	private int amountP; //���������� �����������
	private int amountC; //� ������������
	
	private TableTP tableTP;
	private MethodOfSolution methodOfSolution;
	
	final private JLabel labelProv;
	final private JLabel labelCons;
	final private JLabel labelSupply;
	final private JLabel labelNeed;
	private JLabel [] labelEachProv; //�������� �����������
	private JLabel [] labelEachCons; //�������� ������������
	
	private JLabel [] labelSupplies; //����� �����������
	private JLabel [] labelNeeds; //����������� ������������
	private JLabel [][] labelGoodsTransp; //����������� ������ �� ��������
	
	private JLabel labelIterationNumber; //����� ��������
	private int n; //������� ��������
	
	private JButton buttonOk;
	
	TableResPanel() {
		labelProv = new JLabel("������.");
		labelCons = new JLabel("������.");
		labelSupply = new JLabel("�����");
		labelNeed = new JLabel("�����������");
		
		buttonOk = new JButton("Ok");
		
		//********************��������� �������� ��� ������� ������ ��***************************
		
		buttonOk.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
								
				if (MethodsForTable.isNotDegenerate(tableTP)) { //���� ���� �� �����������
					
					methodOfSolution = OptimalSolution.PotentialMethod(tableTP);
					
					if (methodOfSolution.isOptimalSolution()) //���� ��� ������������� ������ ��������
						
						JOptionPane.showMessageDialog(TableResPanel.this, 
								"������� �������  S = " +  String.valueOf(MethodsForTable.costDelivery(tableTP))); 						
					else {
						
						tableTP = methodOfSolution.findSolution();
						addItems(tableTP, "�������� "+ String.valueOf(n++)); //������� ��������� �� �����;
					}

				}			
				else 
					JOptionPane.showMessageDialog(TableResPanel.this,
							"������� ���� �����������.", "", JOptionPane.ERROR_MESSAGE);					
			}
		});
		//************************************************************************************
	}
	
	void createTableResult(final TableTP tableTP) {
		
		this.tableTP = tableTP;
		amountP = tableTP.getAmountProv();
		amountC = tableTP.getAmountCons();
		n=1;
		
		addItems(tableTP, "������� �������"); //��������� �������� �� panelTable
	}
	
	private void addItems(final TableTP tableTP, String iterationNumber) {
		removeAll();
		labelIterationNumber = new JLabel(iterationNumber +"  S = "
				+ String.valueOf(MethodsForTable.costDelivery(tableTP)));
		//��������� ��������� ���� ��� ����� ��������� ���������
		labelGoodsTransp = new JLabel[amountP][amountC];
		//��������� ���� ��� ����� ������ � �����������
		labelSupplies = new JLabel[amountP];
		labelNeeds = new JLabel[amountC];
		//label � ��������� ����������� � ������������
		labelEachProv = new JLabel[amountP];
		labelEachCons = new JLabel[amountC];
		
		GridBagLayout gblPS = new GridBagLayout();
		setLayout(gblPS);
		GridBagConstraints GBC = new GridBagConstraints();
		//label ���������
		GBC.gridx = 0;
		GBC.gridy = 1;
		gblPS.setConstraints(labelProv, GBC);
		add(labelProv);
		//label �����������
		GBC.gridx = 1;
		GBC.gridy = 0;
		gblPS.setConstraints(labelCons, GBC);
		add(labelCons); 
		//label �����
		GBC.gridx = amountC+2;
		GBC.gridy = 0;
		GBC.insets = new Insets(20,20,0,0);
		gblPS.setConstraints(labelSupply, GBC);
		add(labelSupply);
		//label �����������
		GBC.gridx = 0;
		GBC.gridy = amountP+2;
		GBC.insets = new Insets(20,20,0,0);
		gblPS.setConstraints(labelNeed, GBC);
		add(labelNeed);
		
		//���������� �� ������ ���� ����������� � ������������
		for (int i=0; i<amountP; i++) {
			labelEachProv[i] = new JLabel("A"+(i+1));
			GBC.gridx = 0;
			GBC.gridy = i+2;
			gblPS.setConstraints(labelEachProv[i], GBC);					
			add(labelEachProv[i]);
			
			//label � ���������� ������� ����������
			labelSupplies[i] = new JLabel(String.valueOf(tableTP.getBalanceOfProvider(i)));
			GBC.gridx = amountC+2;
			GBC.gridy = i+2;
			gblPS.setConstraints(labelSupplies[i], GBC);					
			add(labelSupplies[i]);
			
		}
		for (int j=0; j<amountC; j++) {
			labelEachCons[j] = new JLabel("B"+(j+1));
			GBC.gridx = j+2;
			GBC.gridy = 0;
			gblPS.setConstraints(labelEachCons[j], GBC);					
			add(labelEachCons[j]);
			
			//label c ����������� ������������� �����������
			labelNeeds[j] = new JLabel(String.valueOf(tableTP.getBalanceOfConsumer(j)));
			GBC.gridx = j+2;
			GBC.gridy = amountP+2;
			gblPS.setConstraints(labelNeeds[j], GBC);					
			add(labelNeeds[j]);
		}
		
		
		//label � ����������� ������������ ������ 
		for (int i=0; i<amountP; i++)
			for (int j=0; j<amountC; j++) {
				labelGoodsTransp[i][j] = new JLabel(String.valueOf(tableTP.getProductsTransported(i, j)));
				GBC.gridx = j+2;
				GBC.gridy = i+2;
				GBC.weightx = 0.1;
				gblPS.setConstraints(labelGoodsTransp[i][j], GBC);
				add(labelGoodsTransp[i][j]);
			}
		
		//��������� ������ ok
		GBC.gridx = amountC+2;
		GBC.gridy = amountP+2;
		gblPS.setConstraints(buttonOk, GBC);
		add(buttonOk); 
		
		//label ����� ��������
		GBC.gridx = 0;
		GBC.gridy = amountP+3;
		GBC.gridwidth = amountC+3;
		gblPS.setConstraints(labelIterationNumber, GBC);
		add(labelIterationNumber); 
		
		
		revalidate();
		repaint();
	}
}

