package DarkOrion.TPR.TransportationProblemGUI;

import java.awt.BorderLayout;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.MaskFormatter;

class TPFrame extends JFrame {
	
	
	private JPanel statusPanel;
	private TablePrimPanel primaryPanel;
	private TableResPanel resultPanel;
	
	private JLabel labelInvitation;
	private JLabel labelProviders;
	private JLabel labelConsumers;
	private JFormattedTextField textAmountProviders;
	private JFormattedTextField textAmountConsumers;
	private CheckboxGroup grCheckbox;
	private Checkbox chboxVogel;
	private Checkbox chboxNorthWest;
	private JButton buttonOk;
	
	
	TPFrame () {
		
		setTitle("������������ ������");
		setSize(870, 870);
		setLayout(new BorderLayout());
		//����������� ������ ��������� ��� �������� ����������
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//��������� ������-������ �� �����	
		statusPanel = new JPanel();
		statusPanel.setPreferredSize(new Dimension(getWidth(), getHeight()/12));	
		add(statusPanel,BorderLayout.NORTH);
		//������ � ��������
		primaryPanel = new TablePrimPanel();
		primaryPanel.setPreferredSize(new Dimension(getWidth(), 2*getHeight()/5));
		add(primaryPanel,BorderLayout.CENTER);
		//������ � �������������� ��������
		resultPanel = new TableResPanel();
		resultPanel.setPreferredSize(new Dimension(getWidth(), 2*getHeight()/5));
		add(resultPanel,BorderLayout.SOUTH);
	
		addItemsStatusPanel(); //��������� �������� �� statusPanel
		
		setVisible(true);
		
		//�������� ���������� ������������ � �����������
		buttonOk.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int amountP = Integer.valueOf(textAmountProviders.getText());
				int amountC = Integer.valueOf(textAmountConsumers.getText());
				
				//������� ������ tableTP � � �������� ���������� �������� ���������� ����������� � ������������
				
				primaryPanel.createTable(amountP,amountC,resultPanel,chboxVogel.getState(),chboxNorthWest.getState()); //������� ������� �� primaryPanel

			}
		});
	}
	
		
	//�������� �������� �� statusPanel
	private void addItemsStatusPanel() {
		
		labelInvitation = new JLabel("������� ���������� ����������� � ������������ (�� 2 �� 6)");
		labelProviders = new JLabel("�����������:");
		labelConsumers = new JLabel("������������:");
		textAmountProviders = new JFormattedTextField(getFormatTextField());
		textAmountProviders.setValue("4");
		textAmountConsumers = new JFormattedTextField(getFormatTextField());
		textAmountConsumers.setValue("4");
		grCheckbox = new CheckboxGroup();
		chboxVogel = new Checkbox("����� ������",grCheckbox,true);
		chboxNorthWest = new Checkbox("����� ������-��������� ����",grCheckbox,false);
		buttonOk = new JButton("Ok");
		
		GridBagLayout gblPS = new GridBagLayout();
		statusPanel.setLayout(gblPS);
		GridBagConstraints GBC = new GridBagConstraints();
				
		GBC.gridx = 1;
		GBC.gridy = 0;
		gblPS.setConstraints(labelInvitation, GBC);
		statusPanel.add(labelInvitation);
		
		GBC.gridx = 0;
		GBC.gridy = 1;
		gblPS.setConstraints(labelProviders, GBC);
		statusPanel.add(labelProviders);
		
		GBC.gridx = 0;
		GBC.gridy = 2;
		gblPS.setConstraints(labelConsumers, GBC);
		statusPanel.add(labelConsumers);
		
		GBC.gridx = 1;
		GBC.gridy = 1;
		GBC.ipadx = 20;
		gblPS.setConstraints(textAmountProviders, GBC);
		statusPanel.add(textAmountProviders);
		
		GBC.gridx = 1;
		GBC.gridy = 2;
		gblPS.setConstraints(textAmountConsumers, GBC);
		statusPanel.add(textAmountConsumers);
		
		GBC.gridx = 2;
		GBC.gridy = 1;
		GBC.ipadx = 20;
		GBC.anchor = GridBagConstraints.WEST;
		gblPS.setConstraints(chboxVogel, GBC);
		statusPanel.add(chboxVogel);
		
		GBC.gridx = 2;
		GBC.gridy = 2;
		GBC.ipadx = 20;
		GBC.anchor = GridBagConstraints.CENTER;
		gblPS.setConstraints(chboxNorthWest, GBC);
		statusPanel.add(chboxNorthWest);
		
		GBC.gridx = 3;
		GBC.gridy = 2;
		GBC.ipadx = 20;
		gblPS.setConstraints(buttonOk, GBC);
		statusPanel.add(buttonOk);
	}
	
	//���������� �������������� ���  JFormattedTextField
	private MaskFormatter getFormatTextField() {
	    MaskFormatter mf = null;
	    try
	    {
	      mf = new MaskFormatter("#");
	      mf.setValidCharacters("23456");
	      mf.setOverwriteMode(true);
	    }
	    catch (ParseException pe)
	    {
	      System.out.println("error MaskFormatter GetGeoLatFormat()");
	    }
	    
	    return mf;
	}
}
